from django.conf.urls import patterns, include, url
__author__      = "Moataz Soliman"


# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
from remote import views

urlpatterns = patterns('',
    url(r'^$', views.welcome, name='index'),
    (r'^info/$', views.processServerInfo),
)