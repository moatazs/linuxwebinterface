# Create your views here.
__author__      = "Moataz Soliman"


from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import Context, loader
from django.shortcuts import render

from remote.models import File
import paramiko
import re
regex = re.compile("\[([^\]]*)\]\-(.*)")
regex1 = re.compile("([^\/]*)\/(.*)")

def home(request):
	
    final_string = ""

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect('linuxzoo.net', username='root', password='secure')

    stdin, stdout, stderr = ssh.exec_command("tree -if --noreport -sh")

    dir_list = stdout.readlines()

    clean_dir_list = []
    for file in dir_list:
        file = file.replace(" ","")
        file = file.replace("\n","")
        file = file.replace("./","-")
        clean_dir_list.append(file)
        
    # remove the first element because it's "\n" not a real file
    clean_dir_list.pop(0)

    for string in clean_dir_list:
        result = regex.match(string)
        size = result.group(1)	# size in bytes
        remainder = result.group(2)	# rest
        p1 = File(size= size, file_name = remainder)
        p1.save()
        # formating_tabs = ""
        # while(remainder != ''):
            # formating_tabs = formating_tabs + " - "
            # result = regex1.match(remainder)
            # file_name = result.group(1)
            # remainder = result.group(2)
        # print '[%s]\t %s %s' % (size, formating_tabs, file_name)
        # final_string = final_string + '[%s]\t %s %s' % (size, formating_tabs, file_name)
        # final_string = final_string + "\n"
        
        
    ssh.close()
    return HttpResponse(final_string)
    
def welcome(request):
    return render_to_response('welcome.html')
    
#issues : password is visible in url
def processServerInfo(request):
    if 'host' in request.GET and request.GET['host']:
        host = request.GET['host']
    if 'lg' in request.GET and request.GET['lg']:
        lg = request.GET['lg']
    if 'psw' in request.GET and request.GET['psw']:
        psw = request.GET['psw']

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(host, username=lg, password=psw)

    stdin, stdout, stderr = ssh.exec_command("tree -if --noreport -sh")
    
    dir_list = stdout.readlines()
    ssh.close()
    
    clean_dir_list = []
    for file in dir_list:
        file = file.replace(" ","")
        file = file.replace("\n","")
        file = file.replace("./","-")
        clean_dir_list.append(file)
        
    # remove the first element because it's "\n" not a real file
    clean_dir_list.pop(0)

    for string in clean_dir_list:
        result = regex.match(string)
        size = result.group(1)	# size in bytes
        remainder = result.group(2)	# rest
        try:
            file = File.objects.get(file_name = remainder)
            file = File.objects.get(size = size, file_name = remainder)
        except File.DoesNotExist:
            p1 = File(size= size, file_name = remainder)
            p1.save()

        
    latest_list = File.objects.order_by('id')
    template = loader.get_template('dir.html')
    context = {'latest_list': latest_list}
    return render(request, 'dir.html', context)