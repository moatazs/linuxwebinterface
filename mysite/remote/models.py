from django.db import models
__author__      = "Moataz Soliman"

# Create your models here.
class File(models.Model):
    size = models.CharField(max_length=300)
    file_name = models.CharField(max_length=300)