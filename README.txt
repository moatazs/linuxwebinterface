/* 
Moataz Soliman

*/

This small project is a simple a django web-based system that will execute a command on a remote Linux/QNX/UNIX system that lists the contents of directories recursively (relative to the HOME directory) with their size.
The data is then parsed (while maintaining the tree structure) and is loaded as the content of a generated web page.

--------------------------------------------------------------------
for this web application you need:
- django
- paramiko

--------------------------------------------------------------------
For DJANGO:
you can use the pip command from the command line:
# pip install django

or follow the link https://docs.djangoproject.com/en/1.5/topics/install/

--------------------------------------------------------------------
For Paramiko
Paramiko is a SSHLibrary for python on windows

Paramiko can be downloaded from (link: http://www.lag.net/paramiko/). 
Depending on the Python version you have please choose the build.
Paramiko needs Pycrypto to be present on the same machine. 
If your using windoes, installer is available (link: http://www.voidspace.org.uk/python/modules.shtml#pycrypto). This is an exe file which can directly installed int he target machine.

Once Pycrypto is successfully installed, extract the paramiko build in to a folder.

Open command prompt, go to Paramiko root folder.
In the root folder you will find setup.py.

Execute the command :
# python  setup.py build
# python setup.py install

--------------------------------------------------------------------

for testing purposes you can setup a virtual private linux with ssh on Linuxzoo.net
